import { Component, OnInit } from '@angular/core';
import { BattleField } from './battlefield';
import { BattleFieldService } from './battlefield.service';
import { SetupService } from '../setup/setup.service';
import { Setup } from '../setup/setup';
import { Router } from '@angular/router';
import { FinishedGamesService } from '../finished-games/finished-games.service';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';


@Component({
  selector: 'app-battlefield',
  templateUrl: './battlefield.component.html',
  styles: ['./battlefield.component.css']
})
export class BattleFieldComponent implements OnInit {

  public battlefield: BattleField;

  private turnsCount = 0;

  private difficult: Setup;

  constructor(private battlefieldService: BattleFieldService,
    private setupService: SetupService
    , private router: Router,
    private finishedGamesService: FinishedGamesService,
    private toastr: ToastsManager) {
    this.difficult = setupService.getDifficult();

  }

  ngOnInit(): void {
    this.battlefieldService.createBattlefield();
    this.battlefield = this.battlefieldService.getBattleField();
  }

  public attackShip(j: any, k: any): void {
    const row = j;
    const column = k;

    const position = this.battlefield.positions[row][column];

    if (!this.checkValidShot(position)) {
      return;
    }

    // Contador de turnos
    this.turnsCount++;

    if (position.value === 1) {

      this.toastr.success('You shot the ship!', 'Success!');
      this.battlefield.positions[row][column].status = 'win';
      this.battlefield.player.score++;
    } else {
      this.toastr.error('Wrong shoot', 'Uups!');
      this.battlefield.positions[row][column].status = 'fail';
    }
    this.battlefield.positions[row][column].used = true;
    this.battlefield.positions[row][column].value = 'X';
  }

  checkValidShot(position: any): boolean {
    if (this.turnsCount + 1 > this.difficult.turns && this.difficult.turns !== -1) {
      this.toastr.error(`You exceed the maximum of ${this.difficult.turns} turns.`, 'Game over!');
      this.router.navigate(['battlefield']);
      return false;
    }

    if (position.value === 'X') {
      this.toastr.error('You already shot here.', 'Uups!');
      return false;
    }

    return true;
  }
}
