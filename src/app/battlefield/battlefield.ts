import { Player } from "../models/player";

export class BattleField {
  positions: Object[];

  player: Player;

  constructor(values: Object = {}) {
    Object.assign(this, values);
  }

}
