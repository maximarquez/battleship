import { Injectable } from '@angular/core';
import { BattleField } from './battlefield';
import { Player } from '../models/player';


@Injectable()
export class BattleFieldService {

  private playerId = 1;
  battlefield: BattleField;

  constructor() { }

  createBattlefield(): BattleFieldService {
    const battlefieldSize = 10;

    let positions = [];

    for (let i = 0; i < battlefieldSize; i++) {
      positions[i] = [];
      for (let j = 0; j < battlefieldSize; j++) {
        positions[i][j] = { used: false, value: 0, status: '' };
      }
    }

    for (let i = 0; i < battlefieldSize * 2; i++) {
      positions = this.randomShips(positions, battlefieldSize);
    }

    const battlefield = new BattleField({
      player: new Player({ id: this.playerId++ }),
      positions: positions
    });

    this.battlefield = battlefield;
    return this;
  }

  randomShips(positions: Object[], battlefieldSize: number): Object[] {

    battlefieldSize = battlefieldSize - 1;

    const row = this.getRandomInt(0, battlefieldSize);
    const column = this.getRandomInt(0, battlefieldSize);

    if (positions[row][column].value === 1) {
      return this.randomShips(positions, battlefieldSize);
    } else {
      positions[row][column].value = 1;
      return positions;
    }
  }

  getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  getBattleField(): BattleField {
    return this.battlefield;
  }
}
