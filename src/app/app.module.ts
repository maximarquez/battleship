import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AngularFontAwesomeModule } from 'angular-font-awesome';


import { SetupService } from './setup/setup.service';
import { BattleFieldService } from './battlefield/battlefield.service';

import { AppComponent } from './app.component';
import { BattleFieldComponent } from './battlefield/battlefield.component';
import { SetupComponent } from './setup/setup.component';
import { FinishedGamesService } from './finished-games/finished-games.service';
import { FinishedGamesComponent } from './finished-games/finished-games.component';

import {ToastModule} from 'ng2-toastr/ng2-toastr';

const appRoutes: Routes = [
  { path: 'battlefield', component: BattleFieldComponent },
  { path: 'setup/', component: SetupComponent },
  { path: '**', component: SetupComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    BattleFieldComponent,
    SetupComponent,
    FinishedGamesComponent
  ],
  imports: [
    BrowserModule,
    ToastModule.forRoot(),
    BrowserAnimationsModule,
    AngularFontAwesomeModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    )
  ],
  exports: [RouterModule],
  providers: [BattleFieldService, SetupService, FinishedGamesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
