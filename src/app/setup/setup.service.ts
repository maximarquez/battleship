import { Injectable } from '@angular/core';
import { Setup } from './setup';
import { Player } from '../models/player';

@Injectable()
export class SetupService {

  private selectedSetup: Setup;
  private  selectedPlayer: Player;


  private _setups: Setup[] = [{ name: 'Easy', turns: -1 }, { name: 'Medium', turns: 100 },
  { name: 'Hard', turns: 50 }];

  public getAvailableDifficults() {
    return this._setups;
  }

  constructor() { }

  setDifficult(setup: Setup) {
    this.selectedSetup = setup;
  }

  getDifficult() {
    return this.selectedSetup;
  }

  setPlayer(player: Player) {
    this.selectedPlayer = player;
  }

  getPlayer(player: Player) {
    return this.selectedPlayer;
  }

}
