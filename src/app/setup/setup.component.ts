import { Component } from '@angular/core';
import { SetupService } from './setup.service';
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';
import { Setup } from './setup';
import { Router } from '@angular/router';
import { Player } from '../models/player';

@Component({
  selector: 'app-setup',
  templateUrl: './setup.component.html'
})
export class SetupComponent implements OnInit {

  setups: Setup[];

  selectedDifficult: Setup;

  constructor(private setupService: SetupService, private router: Router) {
  }

  ngOnInit(): void {
    this.setups = this.setupService.getAvailableDifficults();
  }

  selectDifficult(setup: Setup) {
    this.selectedDifficult = setup;
  }

  startGame() {
    this.setupService.setDifficult(this.selectedDifficult);
    this.router.navigate(['battlefield']);
  }


}
