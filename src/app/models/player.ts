export class Player {
  public id: number;
  public name: string = 'Player 1';
  public score = 0;

  constructor(values: Object = {}) {
    Object.assign(this, values);
  }
}
