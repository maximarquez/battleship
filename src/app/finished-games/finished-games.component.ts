import { Component } from '@angular/core';
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';
import { Router } from '@angular/router';
import { Player } from '../models/player';
import { BattleField } from '../battlefield/battlefield';
import { FinishedGamesService } from './finished-games.service';

@Component({
  selector: 'app-finished-games',
  templateUrl: './finished-games.component.html'
})
export class FinishedGamesComponent implements OnInit {
  private finishedGames: BattleField[];

  constructor(private router: Router, private finishedGamesService : FinishedGamesService) {
  }

  ngOnInit(): void {
    this.finishedGames = this.finishedGamesService.getFinishedGames();
  }


}
