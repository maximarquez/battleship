import { Injectable } from '@angular/core';
import { BattleField } from '../battlefield/battlefield';

@Injectable()
export class FinishedGamesService {
  private finishedGames: BattleField[];

  constructor() { }

  public getFinishedGames() {
    return this.finishedGames;
  }

  public addFinishedGame(battle: BattleField) {
    this.finishedGames.push(battle);
  }

}
